package id.artivisi.training.microservices.frontend.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WalletServiceConfiguration {

    @Bean
    public WalletRequestInterceptor oauthInterceptor() {
        return new WalletRequestInterceptor();
    }
}
