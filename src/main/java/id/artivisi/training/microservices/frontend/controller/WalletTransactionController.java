package id.artivisi.training.microservices.frontend.controller;

import id.artivisi.training.microservices.frontend.dto.Wallet;
import id.artivisi.training.microservices.frontend.dto.WalletTransaction;
import id.artivisi.training.microservices.frontend.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WalletTransactionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletTransactionController.class);

    @Autowired private WalletService walletService;

    @GetMapping("/wallet/transaction")
    public ModelMap transaksiWallet() {
        Iterable<WalletTransaction> dataTransaksi
                = walletService.ambilDataTransaksiWallet("w001");

        Integer total = 0;
        for (WalletTransaction t : dataTransaksi) {
            System.out.println("Waktu Transaksi : "+t.getTransactionTime());
            System.out.println("Keterangan : "+t.getDescription());
            System.out.println("Nilai : "+t.getAmount());
            total = total + 1;
        }

        LOGGER.info("Mendapatkan {} transaksi untuk wallet {}", total, "w001");

        return new ModelMap()
                .addAttribute("dataTransaksi", dataTransaksi);
    }
}
