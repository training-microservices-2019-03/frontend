package id.artivisi.training.microservices.frontend.service;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;

@Component
public class WalletRequestInterceptor implements RequestInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WalletRequestInterceptor.class);

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        OAuth2AuthenticationToken authentication
                = (OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        OAuth2AuthorizedClient authorizedClient =
                this.authorizedClientService.loadAuthorizedClient(
                        authentication.getAuthorizedClientRegistrationId(),
                        authentication.getName());

        OAuth2AccessToken accessToken = authorizedClient.getAccessToken();
        requestTemplate.header(HttpHeaders.AUTHORIZATION,
                String.format("%s %s", accessToken.getTokenType().getValue(),
                        accessToken.getTokenValue()));

        LOGGER.info("Bearer token type : {}", accessToken.getTokenType().getValue());
        LOGGER.info("Bearer token value : {}", accessToken.getTokenValue());
    }
}
