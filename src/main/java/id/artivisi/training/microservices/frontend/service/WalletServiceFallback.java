package id.artivisi.training.microservices.frontend.service;

import id.artivisi.training.microservices.frontend.dto.WalletTransaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class WalletServiceFallback implements WalletService {
    @Override
    public Iterable<WalletTransaction> ambilDataTransaksiWallet(String walletId) {
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> informasiHostBackend() {
        Map<String, Object> infoDefault = new HashMap<>();

        infoDefault.put("hostname", "not available");
        infoDefault.put("address", "0.0.0.0");
        infoDefault.put("port", "0");

        return infoDefault;
    }
}
